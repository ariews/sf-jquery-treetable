<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Factory\NodeFactory;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        NodeFactory::createMany(10, [
            'kind' => 'directory',
        ]);

        for ($i = 0; $i <= 5; ++$i) {
            NodeFactory::createMany(10, [
                'parent' => NodeFactory::random([
                    'kind' => 'directory',
                ]),
            ]);
        }
    }
}
