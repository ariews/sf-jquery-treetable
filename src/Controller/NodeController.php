<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Node;
use App\Repository\NodeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Routing\Attribute\Route;

class NodeController extends AbstractController
{
    public function __construct(private readonly NodeRepository $repository)
    {
    }

    #[Route('/', name: 'app_node', methods: ['GET'])]
    public function index(): Response
    {
        $nodes = $this->repository->findBy(['parent' => null]);
        $this->sortNodes($nodes);

        return $this->render('node/index.html.twig', [
            'nodes' => $nodes,
        ]);
    }

    #[Route('/children', name: 'app_node_children', methods: ['POST'])]
    public function children(Request $request): Response
    {
        $nodeId = $request->getPayload()->get('nodeId');
        $csrfToken = $request->getPayload()->get('_csrf_token');

        if (empty($nodeId) || !$this->isCsrfTokenValid("n$nodeId", $csrfToken)) {
            throw new AccessDeniedHttpException();
        }

        $nodes = $this->repository->findBy(['parent' => $nodeId]);
        $this->sortNodes($nodes);

        return $this->render('node/nodes.html.twig', [
            'nodes' => $nodes,
            'parentId' => $nodeId,
        ]);
    }

    private function sortNodes(array &$nodes): void
    {
        $sorter = static function (Node $a, Node $b): int {
            if ($a->isDirectory() && !$b->isDirectory()) {
                return -1;
            }

            if ($b->isDirectory() && !$a->isDirectory()) {
                return 1;
            }

            $an = $a->getName();
            $bn = $b->getName();

            $s = [$an, $bn];
            sort($s);

            return array_search($an, $s);
        };

        usort($nodes, $sorter);
    }
}
