<?php

declare(strict_types=1);

namespace App\Factory;

use App\Entity\Node;
use App\Repository\NodeRepository;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;
use Zenstruck\Foundry\RepositoryProxy;

/**
 * @extends ModelFactory<Node>
 *
 * @method        Node|Proxy                     create(array|callable $attributes = [])
 * @method static Node|Proxy                     createOne(array $attributes = [])
 * @method static Node|Proxy                     find(object|array|mixed $criteria)
 * @method static Node|Proxy                     findOrCreate(array $attributes)
 * @method static Node|Proxy                     first(string $sortedField = 'id')
 * @method static Node|Proxy                     last(string $sortedField = 'id')
 * @method static Node|Proxy                     random(array $attributes = [])
 * @method static Node|Proxy                     randomOrCreate(array $attributes = [])
 * @method static NodeRepository|RepositoryProxy repository()
 * @method static Node[]|Proxy[]                 all()
 * @method static Node[]|Proxy[]                 createMany(int $number, array|callable $attributes = [])
 * @method static Node[]|Proxy[]                 createSequence(iterable|callable $sequence)
 * @method static Node[]|Proxy[]                 findBy(array $attributes)
 * @method static Node[]|Proxy[]                 randomRange(int $min, int $max, array $attributes = [])
 * @method static Node[]|Proxy[]                 randomSet(int $number, array $attributes = [])
 */
final class NodeFactory extends ModelFactory
{
    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#factories-as-services
     *
     * @todo inject services if required
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#model-factories
     *
     * @todo add your default values here
     */
    protected function getDefaults(): array
    {
        $kind = self::faker()->randomElement([Node::KIND_F, Node::KIND_D]);

        return [
            'createdAt' => \DateTimeImmutable::createFromMutable(self::faker()->dateTime()),
            'kind' => $kind,
            'modifiedAt' => \DateTimeImmutable::createFromMutable(self::faker()->dateTime()),
            'name' => self::faker()->text(50),
        ];
    }

    /**
     * @see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#initialization
     */
    protected function initialize(): self
    {
        return $this// ->afterInstantiate(function(Node $node): void {})
        ;
    }

    protected static function getClass(): string
    {
        return Node::class;
    }
}
