import './jquery-treetable';

(function ($) {
    const NodeTree = $('#tree');

    NodeTree.treetable({
        expandable: true,
        clickableNodeNames: true,
        onNodeCollapse: function () {
            let node = this;
            NodeTree.treetable("unloadBranch", node);
        },
        onNodeExpand: function () {
            let node = this,
                childUrl = node.row.data('child-url');
            $.ajax({
                method: 'POST',
                async: false, // Must be false, otherwise loadBranch happens after showChildren?
                url: childUrl,
                data: {
                    nodeId: node.id,
                    _csrf_token: node.row.data('csrf')
                }
            }).done(function (html) {
                let rows = $(html).filter("tr");
                NodeTree.treetable("loadBranch", node, rows);
            });
        }
    });
})(jQuery);
